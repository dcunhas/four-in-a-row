from __future__ import division, print_function
import sys
import random
from math import log, sqrt
import datetime
import copy



class RandomBot:
    def get_move(self, pos, time):
        return random.choice(pos.get_legal_moves())


#Monte Carlo Algorithm adapted from https://jeffbradberry.com/posts/2015/09/intro-to-monte-carlo-tree-search/
class MCBot:

    def __init__(self):
        #self.states = [] #Note needed, left in case of future use
        self.calc_time = datetime.timedelta(milliseconds=300)
        self.max_moves = 100
        self.wins = {}
        self.plays = {}

        #Increase to make the AI more explorative, decrease to make it more
        self.C = 10

    def get_move(self, pos, time):
        self.max_depth = 0
        state = pos.board
        player = pos.current_player_id
        print('player %d' %player)
        legal = pos.get_legal_moves()
        if not legal:
            return
        elif len(legal) == 1:
            return legal[0]

        games = 0
        begin = datetime.datetime.utcnow()
        while datetime.datetime.utcnow() - begin < self.calc_time:
            self.simulate(pos)
            games += 1

        #print('games %d' % games)
        move_states = [(m, tuple(tuple(col) for col in pos.simulate_move_no_memory(state, m, in_pId=player))) for m in legal]

        percent_wins, move = max(
            (self.wins.get((player, S), 0) /
             self.plays.get((player, S), 1),
             m)
            for m, S in move_states
        )
        if(percent_wins <= 100 / pos.num_cols):
            print("random choice %.2f" % percent_wins,  file=sys.stderr)
            #for win in move_states:
            #    print(win,  file=sys.stderr)
            return random.choice(legal)
        else:
            return move



    def simulate(self, p):

        #Apparantly this optimizes lookup a bit
        plays, wins = self.plays, self.wins
        visited_states = set()
        pos = copy.deepcopy(p)
        #states_copy = self.states[:]
        state = pos.board

        player = pos.current_player_id

        expand = True
        for t in xrange(1, self.max_moves + 1):

            legal = pos.get_legal_moves()
            arr_move_states = [(m, pos.simulate_move_no_memory(state, m)) for m in legal]
            move_states = [(m, tuple(tuple(col) for col in S)) for m, S in arr_move_states]
            #Check if every move has stats
            if all(plays.get( (player, S) ) for m, S in move_states):
                log_total = log(sum(plays[(player, S)] for m, S in move_states))

                #Choose best move (or branch, based on branching factor self.C)
                value, move, state = max(
                ((wins[(player, S)] / plays[(player, S)]) +
                    self.C * sqrt(log_total / plays[(player, S)]), m, S)
                    for m, S in move_states
                    )
            #Otherwise, random move
            else:
                move, state  = random.choice(arr_move_states)
                #print('rand move %d t: %d' % ( move, t))
            state, winner = pos.simulate_move(move, player)
            #states_copy.append(state)

            t_state = tuple(tuple(col) for col in state)
            if expand and (player, t_state) not in self.plays:
                expand = False
                self.plays[(player, t_state)] = 0
                self.wins[(player, t_state)] = 0

                #If max_moves has not been reached, increase max_depth
                if t > self.max_depth:
                    self.max_depth = t

            visited_states.add((player, t_state))

            #Updatd by simulate_move
            player = pos.current_player_id
            if winner != 0:
                break

        #Update stats
        for player, state in visited_states:
            if(player, state) not in self.plays:
                continue
            self.plays[player, state] += 1
            if player == winner:
                self.wins[(player, state)] += 1

    def set_time_per_move(self, t):
        self.time_per_move = t
        self.calc_time = datetime.timedelta(milliseconds = (t - (0.1 * t)))
