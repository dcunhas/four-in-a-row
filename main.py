def parse_command(instr, bot, pos):
    if instr.startswith('action move'):
        time = int(instr.split(' ')[-1])
        pos.current_player_id = pos.my_player_id
        print('botid %d' %pos.current_player_id)
        x = bot.get_move(pos, time)
        return 'place_disc %d\n' % x
    elif instr.startswith('update game field'):
        fstr = instr.split(' ')[-1]
        pos.parse_field(fstr)
        pos.next_player()
    elif instr.startswith('settings field_columns'):
        pos.set_num_cols(int(instr.split(' ')[-1]))
    elif instr.startswith('settings field_rows'):
        pos.set_num_rows(int(instr.split(' ')[-1]))
    elif instr.startswith('settings your_botid'):
        myid = int(instr.split(' ')[-1])
        bot.myid = myid
        pos.set_player_id(myid)
        bot.oppid = 1 if myid == 2 else 2
        print(pos.my_player_id)
        pos.set_opponent_player_id(bot.oppid)
    elif instr.startswith('settings timebank'):
        bot.timebank = int(instr.split(' ')[-1])
    elif instr.startswith('settings time_per_move'):
        bot.set_time_per_move(int(instr.split(' ')[-1]))
    return ''

if __name__ == '__main__':
    import sys
    from position import Position
    from bot import MCBot

    pos = Position()
    bot = MCBot()

    while True:
        try:
            instr = raw_input()
        except Exception as e:
            sys.stderr.write('error reading input')
        outstr = parse_command(instr, bot, pos)
        sys.stdout.write(outstr)
        sys.stdout.flush()
