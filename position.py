import copy

class Position:

    def __init__(self):
        #self.board = [][]
        self.num_rows = 0
        self.num_cols = 0
        self.last_col = -1
        self.last_err = ""
        self.current_player_id = 0
        self.my_player_id = 0
        self.opponent_player_id = 0

    def parse_field(self, fstr):
        strs = fstr.split(';')
        flist = [f.split(',') for f in strs]
        self.board = [ [int(f) for f in a]for a in flist ]

    def make_clean_board(self):
        self.board = [[0 for col in range(self.num_cols)] for row in range (self.num_rows)]

    def set_num_rows(self, x):
        self.num_rows = x;
        if self.num_cols > 0:
            self.make_clean_board()

    def set_num_cols(self, x):
        self.num_cols = x
        if self.num_rows > 0:
            self.make_clean_board()

    def getDisk(col, row):
        return self.board[col][row]

    def make_move(self, col, disk):
        self.last_err = ""
        if(col < self.num_cols):
            for y in range(num_rows):
                if self.board[col][y] == 0:
                    self.board[col][y] == disk
                    self.last_col = col
                    self.current_player_id = self.my_player_id
                    return True
            self.last_err = "Column is full"
        else:
            self.last_err = "Move out of bounds"
        return False

    def get_legal_moves(self):
        moves = []
        for ind, m in enumerate(self.board[0]):
            if m == 0:
                moves.append(ind)
        return moves

    def get_board(self):
        return ''.join(self.board, ',')

    #Move column, playerId
    #def si

    def simulate_move(self, j, pId):
        self.last_err = ""
        if j < 0 or j >= self.num_cols:
            self.last_err = "Index out of bounds"
            return self.board, 0
        if self.board[0][j] != 0:
            self.last_err = "Column Full"
            return self.board, 0
        #Place piece at lowest row in column
        row = 0
        for i in reversed(range(self.num_rows)):
            if self.board[i][j] == 0:
                self.board[i][j] = pId
                row = i
                break
        self.next_player()
        return self.board, self.winner_from_pos(row = row, col = j, board = self.board)


    def simulate_move_no_memory(self, in_board, j, in_pId = None):
        board = copy.deepcopy(in_board)
        if in_pId == None:
            pId = self.current_player_id
        else:
            pId = in_pId
        self.last_err = ""
        if j < 0 or j >= self.num_cols:
            self.last_err = "Index out of bounds"
            return board
        if board[0][j] != 0:
            self.last_err = "Column Full"
            return board
        #Place piece at lowest row in column
        row = 0
        for i in reversed(range(self.num_rows)):
            if board[i][j] == 0:
                board[i][j] = pId
                row = i
                break
        return board#, self.winner_from_pos(row = row, col = j, board = self.board)


    #Player id if winner, otherwise 0
    #Row, col are last played piece
    #Used algorithm from https://codereview.stackexchange.com/questions/112948/checking-for-a-win-in-connect-four
    def winner_from_pos(self, row, col, board):
        if board[row][col] == 0:
            return 0
        piece = board[row][col]
        for d_row, d_col in [(1, 0), (0, 1), (1, 1), (1, -1)]:
            consec = 1
            for d in (1, -1):
                d_row *= d
                d_col *= col
                next_row = row + d_row
                next_col = col + d_col
                while 0 <= next_row < self.num_rows and 0 <= next_col < self.num_cols:
                    if board[next_row][next_col] == piece:
                        consec += 1
                    else:
                         break
                    if consec == 4:
                        return piece
                    next_row += d_row
                    next_col += d_col
        return 0
    def next_player(self):
        if self.current_player_id == 0:
            self.last_err = "Need first player first"
            return
        elif self.current_player_id == self.my_player_id:
            self.current_player_id = self.opponent_player_id
        else:
            self.current_player_id = self.my_player_id

    def get_board_array(self):
        return self.board
    def set_player_id(self, id):
        self.my_player_id = id
    def set_opponent_player_id(self, id):
        self.opponent_player_id = id
    def set_time_per_move(self, x):
        self.time_per_move = x
